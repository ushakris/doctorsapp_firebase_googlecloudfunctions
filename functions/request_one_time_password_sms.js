const admin = require('firebase-admin');
const client = require('./sms_gateway');

module.exports = function(req, res) {
  if (!req.body.phone) {
    return res.status(422).send({ error: 'You must provide a phone number' });
  }

  const phone = String(req.body.phone).replace(/[^\d]/g, '');

  admin.auth().getUser(phone)
    .then(userRecord => {
      const code = Math.floor((Math.random() * 8999 + 1000));

      const msg = "Hello world. " + code;
      client.Message.send({
        from : "+13232726226",    //"+12525089000" // This must be a Catapult number on your account
        to   : "+19342227181",    //must be changed to dynamic variable phone
        text : msg
      })
      .then(function(message) {
        console.log("Message sent with ID " + message.id);
          admin.database().ref('testusers/' + phone)
            .update({ code: code, codeValid: true }, () => {
              res.send({ success: true });
          });
      })
      .catch(function(err) {
        console.log(err.message);
      });
    })
    .catch((err) => {
      res.status(422).send({ error: err });
    });
}
